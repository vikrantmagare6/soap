<?php

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Text to send if user hits Cancel button';
    exit;
} 
if (extension_loaded('soap'))
{
    // Request parameters :
    // Exemple is a Nav Code unit GetSalesPrices, method GetPrice(CodPCustomerNo, CodPItemNo)
    $NavUsername = "rte";
    $NavAccessKey = "ff@4321";
    $CodeunitMethod = "CreateCustomer";
    $params = array(
        "rTCWebCustCode" => "0222",
        "rTCWebEmailID" => "adfa@gmail.com",
    );
    // SOAP request header
    $url = "http://110.173.189.18:3030/FEPL2016LIVE/WS/TEST_FEPL_1920_30122019/Codeunit/RTCEcommWS";
    
    $options = array(
        'authentication' => SOAP_AUTHENTICATION_BASIC,
        'login' => $NavUsername,
        'password' => $NavAccessKey,
        'trace' => 1,
        'exception' => 0,
    );
    try
    {
        $client = new SoapClient($url, $options);
                
        $soap_response = $client->__soapCall($CodeunitMethod, array('parameters' => $params));
        echo "SOAP REQUEST SUCESS :";
        
    }
    catch (SoapFault $soapFault)
    {
        // echo "SOAP REQUEST FAILED :<br>";
        echo $soapFault->getMessage();
        // echo "Request :<br>" . htmlentities($soap_client->__getLastRequest()) . "<br>";
        // echo "Response :<br>" . htmlentities($soap_client->__getLastResponse()) . "<br>";
    }
}
else
{
    echo "Php SOAP extention is not available. Please enable/install it to handle SOAP communication.";
}
?>